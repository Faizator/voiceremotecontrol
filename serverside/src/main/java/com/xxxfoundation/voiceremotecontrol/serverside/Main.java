package com.xxxfoundation.voiceremotecontrol.serverside;

import com.xxxfoundation.networktools.SimpleUDPServer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.util.Arrays;

/**
 * Код серверной части (которая запущена на компьютере, принимающем сообщения)
 */
public class Main {
    public static void main(String[] args) {
        final SimpleUDPServer server = new SimpleUDPServer(3000); // привязываем сервер-слушатель к 3000 порту

        server.addListener(new SimpleUDPServer.IServerListener() {
            @Override
            public void onServerStart() {
                System.out.println("SERVER STARTED");
            }

            @Override
            public void onServerPacketReceive(DatagramPacket packet) {
                try {
                    String msg = new String(Arrays.copyOf(packet.getData(), packet.getLength()), "UTF-8");
                    System.out.println("RECV: " + msg);
                    processReceivedMessage(msg);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServerDataReceive(byte[] data) {

            }

            @Override
            public void onServerStop() {
                System.out.println("SERVER STOPPED");
            }
        });

        server.start();
    }

    public static void processReceivedMessage(String msg) {
        System.out.println(msg);
        //... здесь код обработки сообщения с телефона
    }
}

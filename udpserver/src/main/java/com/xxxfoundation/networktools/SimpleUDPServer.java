package com.xxxfoundation.networktools;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by FAZ on 21.01.2017.
 */
public class SimpleUDPServer {
    public static class DSocketFactory {
        public DatagramSocket createSocket() {
            try {
                return new DatagramSocket(null);
            } catch (SocketException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private int port;
    private ArrayList<IServerListener> listeners = new ArrayList<>();
    private volatile boolean isRunning = false;
    DatagramSocket serverSocket;
    DSocketFactory dSocketFactory = new DSocketFactory();
    Thread receivingTaskThread;

    public SimpleUDPServer(int port) {
        this.port = port;
    }

    public SimpleUDPServer setDSocketFactory(DSocketFactory factory) {
        this.dSocketFactory = factory;
        return this;
    }

    public synchronized boolean start() {
        if ( isRunning() ) {
            return true;
        }
        boolean init = initialize();
        if( init ) {
            return true;
        }
        return false;
    }

    private boolean initialize() {
        isRunning = true;
        try {
            serverSocket = dSocketFactory.createSocket();
            serverSocket.setBroadcast(true);
            serverSocket.bind(new InetSocketAddress(Inet4Address.getByName("0.0.0.0"), this.port));
            serverListenerNotifier.onServerStart();
            startReceiving();
            return true;
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        stop();
        return false;
    }

    private void startReceiving() {
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        executorService.submit(receivingTask);
        receivingTaskThread = new Thread( receivingTask );
        receivingTaskThread.start();
    }

    public boolean send(DatagramPacket packet) {
        if( !isRunning() ) {
            return false;
        }
        try {
            serverSocket.send(packet);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        stop();
        return false;
    }

    private Runnable receivingTask = new Runnable() {
        @Override
        public void run() {
            byte[] buff = new byte[1024];
            DatagramPacket packet;
            while( isRunning() && !Thread.currentThread().isInterrupted()) {
                try {
                    packet = new DatagramPacket(buff, buff.length);
                    serverSocket.receive(packet);
                    serverListenerNotifier.onServerPacketReceive(packet);
                } catch (Exception e) {
//                    e.printStackTrace();
                    break;
                }
            }
            stop();
        }
    };

    public synchronized void stop() {
        if( !isRunning ) {return;}
        isRunning = false;
        serverListenerNotifier.onServerStop();
//        removeAllListeners();
        receivingTaskThread.interrupt();
        serverSocket.close();
    }


    private ServerListenersNotifier serverListenerNotifier = new ServerListenersNotifier();

    public boolean isRunning() {
        return isRunning;
    }

    private class ServerListenersNotifier implements IServerListener {

        @Override
        public void onServerStart() {
            for( IServerListener listener : listeners ){
                listener.onServerStart();
            }
        }

        @Override
        public void onServerDataReceive(byte[] data) {
            for( IServerListener listener : listeners ){
                listener.onServerDataReceive(data);
            }
        }

        @Override
        public void onServerPacketReceive(DatagramPacket packet) {
            for( IServerListener listener : listeners ){
                listener.onServerPacketReceive(packet);
                listener.onServerDataReceive(packet.getData());
            }
        }

        @Override
        public void onServerStop() {
            for( IServerListener listener : listeners ){
                listener.onServerStop();
            }
        }
    }

    public interface IServerListener {
        void onServerStart();
        void onServerPacketReceive(DatagramPacket packet);
        void onServerDataReceive(byte[] data);
        void onServerStop();
    }

    public static class ServerListenerAdapter implements IServerListener {
        @Override
        public void onServerStart() {}
        @Override
        public void onServerPacketReceive(DatagramPacket packet) {}
        @Override
        public void onServerDataReceive(byte[] data) {}
        @Override
        public void onServerStop() {}
    }

    public synchronized void addListener(IServerListener listener) {
        removeListener(listener);
        listeners.add(listener);
    }

    public synchronized void removeListener(IServerListener listener) {
        while( listeners.remove(listener) ) {}
    }

    public int getListenersSize() {
        return listeners.size();
    }

    public synchronized void removeAllListeners() {
        listeners.clear();
    }
}

package com.xxxfoundation.voiceremotecontrol;

import android.app.SearchManager;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.xxxfoundation.networktools.SimpleUDPServer;


import java.nio.charset.Charset;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements IMainInteraction {
    final static int VOICE_RECOGNITION_REQUEST_CODE = 1;

    @BindView(R.id.voiceCommandBtn) Button voiceCommandBtn;
    MainPresenter mainPresenter;

    volatile boolean serverIsReady = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mainPresenter = new MainPresenter();
        voiceCommandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voiceCommandBtn.setEnabled(false);
                if( serverIsReady ) {
                    speak();
                } else {
                    mainPresenter.startServer();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.attach(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mainPresenter.detach();
    }


    public void speak() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ru-RU");
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        voiceCommandBtn.setEnabled(true);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                ArrayList<String> textMatchList = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                onSpeechRecognizeSuccess(textMatchList);
                return;
            }
        }
        onSpeechRecognizeFail();
    }

    /**
     * Обработчик успешного распознания голоса.
     * @param result массив распознанных вариантов
     */
    void onSpeechRecognizeSuccess(ArrayList<String> result) {
        System.out.println(result);
        if( result.size() > 0 ) {
            if( !mainPresenter.sendVoiceMessagePacket(result.get(0)) ) { // отправляем содержимого первого из вариантов распознания голосового сообщения бродкастом в сеть
                Toast.makeText(this, "Ошибка при отправке сообщения", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Обработчик неудачного распознавания голоса.
     */
    void onSpeechRecognizeFail() {
        System.out.println("FAIL");
        Toast.makeText(this, "Не удалось распознать текст", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerStart() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                serverIsReady = true;
                voiceCommandBtn.setEnabled(true);
                voiceCommandBtn.setText(R.string.voice_command);
                voiceCommandBtn.setBackgroundResource(R.drawable.round_green_button);
            }
        });
    }

    @Override
    public void onServerStop() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                serverIsReady = false;
                voiceCommandBtn.setEnabled(true);
                voiceCommandBtn.setText(R.string.start_server);
                voiceCommandBtn.setBackgroundResource(R.drawable.round_red_button);
            }
        });
    }

    @Override
    public void onServerStarting() {

    }

    @Override
    public void onServerStopping() {

    }

    @Override
    public void onConnectionLost() {

    }

    @Override
    public void onLog(String msg) {

    }
}

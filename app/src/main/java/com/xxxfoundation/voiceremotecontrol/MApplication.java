package com.xxxfoundation.voiceremotecontrol;

import android.app.Application;
import android.util.Log;
import com.xxxfoundation.networktools.SimpleUDPServer;

import java.net.DatagramPacket;

/**
 * Created by FAZ on 22.01.2017.
 */

public class MApplication extends Application {

    private static MApplication instance;
    private SimpleUDPServer server;


    public static MApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public SimpleUDPServer getServer() {
        if( server == null ) {
            server = new SimpleUDPServer(9999);
            server.addListener(appServerListener);
        }
        return server;
    }

    private SimpleUDPServer.IServerListener appServerListener = new SimpleUDPServer.IServerListener() {
        @Override
        public void onServerStart() {
            log("SERVER STARTED");
        }

        @Override
        public void onServerPacketReceive(DatagramPacket packet) {
            log("PACKET RECEIVED");
        }

        @Override
        public void onServerDataReceive(byte[] data) {
            log("DATA RECEIVED");
        }

        @Override
        public void onServerStop() {
            log("SERVER STOPPED");
        }

    };

    public static void log(String msg ){
        Log.d("TAG", msg);
    }
}

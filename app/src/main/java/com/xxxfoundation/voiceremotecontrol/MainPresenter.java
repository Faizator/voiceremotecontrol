package com.xxxfoundation.voiceremotecontrol;

import android.util.Log;

import com.xxxfoundation.networktools.SimpleUDPServer;
import com.xxxfoundation.voiceremotecontrol.helpers.NetworkHelper;

import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by FAZ on 28.01.2017.
 */
public class MainPresenter {
    IMainInteraction interaction;
    SimpleUDPServer serverInstance;
    List localAddesses;
    InetAddress currentDiscoveredAddress;
    int defaultSendingPort = 3000;

    public void setDefaultSendingPort(int port) {
        this.defaultSendingPort = port;
    }

    private IMainInteraction interationWrapper = new IMainInteraction() {
        @Override
        public void onServerStart() {
            if( interaction != null ) { interaction.onServerStart(); }
        }

        @Override
        public void onServerStop() {
            if( interaction != null ) { interaction.onServerStop(); }
        }

        @Override
        public void onServerStarting() {
            if( interaction != null ) { interaction.onServerStarting(); }
        }

        @Override
        public void onServerStopping() {
            if( interaction != null ) { interaction.onServerStopping(); }
        }

        @Override
        public void onConnectionLost() {
            if( interaction != null ) { interaction.onConnectionLost(); }
        }

        @Override
        public void onLog(String msg) {
            if( interaction != null ) { interaction.onLog(msg); }
        }
    };

    public MainPresenter() {
        this(MApplication.getInstance().getServer());
    }

    public MainPresenter(SimpleUDPServer serverInstance){
        this.serverInstance = serverInstance;
        registerRemoteAnswerProcessors();
    }

    public void attach(IMainInteraction interaction) {
        this.interaction = interaction;
        serverInstance.addListener(serverListener);

        notifyInteractionCurrentState();
    }

    void notifyInteractionCurrentState() {
        if( serverInstance.isRunning() ) {
            interationWrapper.onServerStart();
        } else {
            interationWrapper.onServerStop();
        }
    }

    public void detach() {
        this.interaction = null;
        serverInstance.removeListener(serverListener);
    }

    public void toggleServer() {
        if( serverInstance.isRunning() ) {
            stopServer();
        } else {
            startServer();
        }
    }

    public void startServer() {
        if( serverInstance.isRunning() ) {
            notifyInteractionCurrentState();
            return;
        }
        interationWrapper.onServerStarting();
        serverInstance.start();
    }

    public void stopServer() {
        if( !serverInstance.isRunning() ) {
            notifyInteractionCurrentState();
            return;
        }
        interationWrapper.onServerStopping();
        serverInstance.stop();
    }

    public boolean sendVoiceMessagePacket(String msg) {
        return sendDefaultBroadcastPacket(msg);
    }

    public boolean sendDefaultBroadcastPacket(String msg) {
        return sendPacket(msg, NetworkHelper.getBroadcastAddress(), defaultSendingPort);
    }

    public boolean sendPacket(String msg, InetAddress address, int port) {
        final DatagramPacket packet;
        try {
            byte[] msgBytes = msg.getBytes("UTF-8");
            packet = new DatagramPacket(msgBytes, msgBytes.length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
        packet.setAddress(address);
        packet.setPort(port);

        if( !serverInstance.isRunning() ) {
            log("SERVER NOT RUNNING");
            return false;
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Boolean> future = executor.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return serverInstance.send(packet);
            }
        });
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    private SimpleUDPServer.IServerListener serverListener = new SimpleUDPServer.IServerListener() {
        @Override
        public void onServerStart() {
            log("SERVER START ACT");
            interationWrapper.onServerStart();
        }

        @Override
        public void onServerPacketReceive(DatagramPacket packet) {
            String msg = new String(Arrays.copyOf(packet.getData(), packet.getLength()));
            log("PACKET RECV: " + msg + " FROM " + packet.getAddress());
            if( getLocalIpAddresses().contains( packet.getAddress().toString().substring(1) ) ) {
                log("RECEIVED FROM MYSELF");
            } else {
                log("RECEIVED FROM REMOTE SERVER " + msg );
                processRemoteAnswer(packet.getAddress(), msg);
            }
        }

        @Override
        public void onServerDataReceive(byte[] data) {
            log("DATA RECEIVE ACT");
        }

        @Override
        public void onServerStop() {
            log("SERVER STOP ACT");
            interationWrapper.onServerStop();
        }
    };


    private void registerRemoteAnswerProcessors() {
        registeredRemoteAnswerProcessors = new ArrayList<>();
        registeredRemoteAnswerProcessors.add(new IRemoteAnswerProcessor() {       // DISCOVER_ANSWER
            @Override
            public void processRemoteAnswer(InetAddress address, String answer) {
                if( answer.startsWith("...") ) {
                    //...
                }
            }
        });
    }

    private void printAddresses() {
        log(getLocalIpAddresses().toString());
    }

    private void processRemoteAnswer(InetAddress address, String msg) {
        for( IRemoteAnswerProcessor remoteAnswerProcessor : registeredRemoteAnswerProcessors ) {
            remoteAnswerProcessor.processRemoteAnswer(address, msg);
        }
    }

    private ArrayList<IRemoteAnswerProcessor> registeredRemoteAnswerProcessors = new ArrayList();

    interface IRemoteAnswerProcessor {
        void processRemoteAnswer(InetAddress address, String answer);
    }

    private List getLocalIpAddresses()
    {
        if( localAddesses != null ) {
            return localAddesses;
        }

        localAddesses = NetworkHelper.getLocalIpAddresses();
        return localAddesses;
    }

    public void log(String msg) {
        Log.d("TAG", msg);
        interationWrapper.onLog(msg);
    }
}

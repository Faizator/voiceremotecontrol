package com.xxxfoundation.voiceremotecontrol;

/**
 * Created by FAZ on 28.01.2017.
 */

public interface IMainInteraction {
    void onServerStart();
    void onServerStop();
    void onServerStarting();
    void onServerStopping();
    void onConnectionLost();
    void onLog(String msg);
}
